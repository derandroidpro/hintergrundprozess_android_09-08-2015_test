package de.derandroidpro.backgroundprocess09_08_2015beta;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.*;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class Notificationservice extends Service {

    final int NOTIFICATIONID = 3;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
               notificationAnzeigen();
            }
        }, 1000*60*2); // Die Verzögerung wird in Millisekunden angegeben: 1000*60*2 ms = 120 000 ms = 2 Minuten.


        Log.d("Service runned", "onStartCommand ausgefuehrt!");
        return super.onStartCommand(intent, flags, startId);

    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void notificationAnzeigen (){
        Bitmap largenotificon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification.Builder notification = new Notification.Builder(this);
        notification.setContentTitle("Deine Benachrichtigung");
        notification.setContentText("Tippe, um die App zu öffnen");
        notification.setSmallIcon(R.drawable.notif_small_icon1);
        notification.setLargeIcon(largenotificon);
        notification.setAutoCancel(true);

        Intent startAppIntent = new Intent(Notificationservice.this,MainActivity.class);
        PendingIntent startapppendingintent = PendingIntent.getActivity(getApplicationContext(),0, startAppIntent,0);
        notification.setContentIntent(startapppendingintent);

        notificationManager.notify(NOTIFICATIONID, notification.build());

        stopSelf(); // mit StopSelf(); wird der Service beendet. Wenn der Service nicht mehr aktiv ist,
                    // sollte man ihn beenden, weil der Service sonst unnötig Arbeitsspeicher verbraucht.

    }
}
