package de.derandroidpro.backgroundprocess09_08_2015beta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent servicerunintent = new Intent(MainActivity.this, Notificationservice.class);
                startService(servicerunintent);

                // Ein Service ist ein Hintergrundprozess in Android. Dieser läuft auch weiter, wenn man die App beendet.

                Log.d("Button onclick", "Button wurde gedrückt");
            }
        });
    }


}
